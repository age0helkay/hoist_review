# -*- coding: utf-8 -*-
import os
import lib.common as c
from itertools import chain

properties = {
'requires_root': False,
'requires_network': False
}


description = '''
Module checks /etc directory for any of the predefined critical files and their
permissions. Also gathers all writable files in /etc as anything could be of intrest
as usually nothing should be writable for non root users.
'''

# Predefined critical files
# TODO: might be other files that are crit read apart from the obvious shadow
#       check the crit for uncommon perm patterns and owners
#       check other things that are read sens
#       phpmyadmin
#       mysql?
#       samba
crit_config_files = ['/etc/inittab', '/etc/inetd.conf', '/etc/xinetd.conf',
                     '/etc/contab','/etc/fstab',' /etc/profile',
                     '/etc/sudoers','/etc/cron','/etc/init',
                     '/etc/bash.bashrc','/etc/inetd','/etc/xinetd',
                     '/etc/hosts.equiv', '/etc/hosts.equiv', '/etc/shosts.equiv']

def main(section):
    w_crit_conf = []
    w_other = []
 

    for dirname, dirnames, filenames in os.walk('/etc'):
        
      # List of directories and files in /etc
      for f in chain(filenames,dirnames):
          full_path = os.path.join(dirname,f)
          
          # if file is writable
          if c.is_writable(full_path):
              
              # if file in predefined list add to special buff
              if full_path in crit_config_files:
                  w_crit_conf.append(os.path.join(dirname,f))
              else:
                  # File is writable but not predefined critical
                  w_other.append(os.path.join(dirname,f))
    
    section.add_issue('Found %s predefined critical writable configuration files.' 
    % len(w_crit_conf))
    
    section.add_issue(''.join(['=> '+x+'\n' for x in w_crit_conf]))
    
    section.add_issue('Found %s other writable configuration files.' 
    % len(w_other))
    
    section.add_issue(''.join(['=> '+x+'\n' for x in w_other]))
    
    
    return section
    
    

