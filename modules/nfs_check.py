import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}


description = '''
Checks for nfs mounts and exports
    TODO: clean up code
    Actually check the mount and export options
    
    ref:
        http://www.centos.org/docs/5/html/Deployment_Guide-en-US/s1-nfs-client-config.html
'''

def main(section):
    
    # Client checks
    # check for nfs mounts in /proc/mounts and fstab
    nfs_client_entrys = []
    mount_files = ['/proc/mounts', '/etc/fstab']
    for path in mount_files:
        for line in (x for x in c.read_comment_free_lines(path) if 'nfs' in x):
            nfs_client_entrys.append('Found entry matching \'nfs\' in file \'%s\'\n=> \'%s\'\n' % (path, line))
    if len(nfs_client_entrys)>0:
        section.add_issue('This host is an NFS client:')
        section.add_issue(''.join(nfs_client_entrys))
        
    # Server checks
    # TODO: might extract nfs stats from /proc/self/mountstats
    # http://utcc.utoronto.ca/~cks/space/blog/linux/NFSMountstatsIndex
    nfs_exports = c.read_comment_free_lines('/etc/exports')
    if len(nfs_exports)>0:
        section.add_issue('This host is an NFS server. Found %s entries in /etc/exports:' %
        len(nfs_exports))
        section.add_issue(''.join('=> ' + x + '\n' for x in nfs_exports))
    
    if((len(nfs_client_entrys)+len(nfs_exports))<1):
        section.add_issue('Found no NFS configuration entries.')
    return section


    
    
