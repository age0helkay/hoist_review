import os, glob
import lib.common as c
from itertools import chain


properties = {
'requires_root': False,
'requires_network': False
}

description = '''
Module checks filepermissions related to services and startup scripts
* Checks for readable/writable scripts and configurations in init, inittab, init.d  & xinetd.d
* Checks for writable file refs in all configurations and scripts
* Symlinks pointing outside of conventional directories

TODO: Systemd in ist own module?

'''


def main(section):
    
    writable = []
    writable_refs = []
    
    basedirs = ['/etc/init.d','/etc/rc.d', '/etc/systemd',
                '/usr/local/etc/rc.d', '/etc/init', '/etc/xinetd.d']
    # rc?.d contains symlinks by convention but might be scripts if someone goofed
    basedirs.extend(glob.glob('/etc/rc?.d/'))
    file_collection = ['/etc/rc.local','/etc/inittab', '/etc/xinetd.conf']
    
    # Collect files to check
    for path, dirs, files in chain.from_iterable(
    os.walk(path) for path in basedirs):
        for fullpath in (os.path.join(path,files) for files in files):
            # Follow links
            if os.path.islink(fullpath): fullpath = os.path.realpath(fullpath)
            
            file_collection.append(fullpath)
    
    # sort out dups
    file_collection = sorted(set(file_collection))  
    
    # Check file perms
    for path in [x for x in file_collection if c.is_writable(x)]:
        writable.append('Found writable file \'%s\' owned by %s.' % (
            path, c.get_owner(path)))
    
    # Check ref and PATH var file perm
    for path in file_collection:
        for fileref in [ x for x in c.extract_paths(path) if c.is_writable(x) ]:
            writable_refs.append('Found ref to writable file \'%s\' owned by %s in file \'%s\'.' % (
            fileref, c.get_owner(fileref), path))
            
    section.add_issue('Found %s writable service and init related files.' % len(writable))
    section.add_issue(''.join(['=> '+x+'\n' for x in writable]))
    section.add_issue('Found %s references to writable files in service and init related files.' 
    % len(writable_refs))
    section.add_issue(''.join(['=> '+x+'\n' for x in writable_refs]))
    return section

    
