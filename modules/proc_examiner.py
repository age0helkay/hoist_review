import os, pwd
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}


description = '''
Module sifts through running processes with effective uid other than self and 
tries to identify various things related to them by reading /proc/#/cmdline

* Checks if running program file is writable
* Checks if any file arguments are writabe
* If running program is a known interpreter it checks argument file for read and write permission
* If running programs argument is a script it checks for checks for read and write permission

TODO: Check file descriptors in fd
      smaps
    
ref: http://en.wikipedia.org/wiki/List_of_command-line_interpreters

'''
# Common basedirs for binaries
common_bin_locations = ['/usr/sbin', '/sbin', '/usr/local/bin', 
                        '/usr/local/sbin', '/usr/bin', '/bin', '/usr/libexec']

# TODO: more script types? jar?
script_patterns = ('.py','.sh','.bash','.zsh','.rb','.pl', '.lua', '.php', '.tcl')

# TODO: other relevant interpreters or common naming conventions of diff version?
interpreter_patterns = ('/bash', '/python', '/python2.7','/python2.6','/python2.5',
                        '/python2.4','/python2.3','/python2.2', '/python2.1', 
                        '/ruby', '/sh', '/zsh', '/csh','/php', '/ash', '/dash', 
                        '/ash', '/ch', '/ksh')
                        
                        

def _determine_path(potential_path):
    '''
    Checks if supplied path is a file, if path is not abs tries to guess path
    returns only valid paths to existing files
    '''
    
    # Check if path is absolute/existing path to existing file
    if os.path.isfile(potential_path):
        return potential_path
    else: # Try to guess our way to binary path
          # TODO: solution for paths in lib eg. udisks-daemon: -> /usr/lib/udisks/udisks-daemon
          # TODO: Other common chars than : that shows up in cmdline that need to be stripped?
        cleaned_potential_path = potential_path.replace(':','')
        # Check common locations for binaries
        for directory in common_bin_locations:
            guessed_path = os.path.join(directory, cleaned_potential_path)
            if os.path.isfile(guessed_path):
                return guessed_path
        # if none of the guessed paths work return cmdline path
        return potential_path

def _get_proc_uids(pid):
    ''' returns ruid and euid for pid '''
    for line in c.read_file(os.path.join('/proc',pid,'status')).split('\n'):
        if 'Uid' in line:
            uid_vals = line.split('\t')
            #Uid: real UID, effective UID, saved set UID, and file system UID
            ruid = uid_vals[1]
            euid = uid_vals[2]
            
            return (ruid,euid)
            
def _get_process_info(): 
    ''' Returns pid, cmdline, ruid, euid on all processes '''
    processes = []
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
    for pid in pids:
        uids = _get_proc_uids(pid)
        processes.append({'pid': pid, 
                          'cmdline' : _get_cmdline(pid),
                          'ruid' : uids[0],
                          'euid' : uids[1]
                          })
    return processes
    

def _perm_string(r,w):
    ''' Helper for info string building '''
    if r and w:
        return 'readable and writable'
    if r:
        return 'readable'
    if w:
        return 'writable'
        
def _get_cmdline(pid):
    '''
    Return cmdline of pid
    TODO: Resolve from status name field if cmdline is empty?
    '''
    return c.read_file(os.path.join('/proc',pid,'cmdline')).replace('\0',' ')

def main(section):
    writable_buf = []
    script_match_buf = []
    
    
    for proc in _get_process_info():
        if proc['euid'] == str(os.getuid()): continue # Skip procs owned by self
        if proc['cmdline']: # Check that cmdline wasnt empty

            # Make sure first part of cmdline points to existing file
            path = _determine_path(proc['cmdline'].split(' ')[0])
                        
            # Separate args
            arguments = proc['cmdline'].split(' ')[1:]
            
            # if program path can be determined
            if path:
                
                # If running program is writable
                if c.is_writable(path): 
                    writable_buf.append("PID %s with EUID %s (%s) is running a writable program '%s'." % (
                    proc['pid'],proc['euid'], pwd.getpwuid(int(proc['euid'])).pw_name, path))
                
                # Check if any arguments are writable files 
                for arg in (
                arg for arg in arguments if os.path.isfile(arg) 
                and c.is_writable(arg)):
                    writable_buf.append("PID %s with EUID %s (%s) running program '%s' has a writable file argument '%s'." % (
                    proc['pid'], proc['euid'], pwd.getpwuid(int(proc['euid'])).pw_name, path, arg))
                    
                # If running program is a known interpreter and arg is file 
                for arg in (
                arg for arg in arguments if os.path.isfile(arg)
                and path.endswith(interpreter_patterns)):
                     w, r = c.is_writable(arg), c.is_readable(arg)
                     if w or r:
                         script_match_buf.append("PID %s with EUID %s (%s) running '%s' matching interpreter pattern file argument '%s' is %s." % (
                         proc['pid'], proc['euid'], pwd.getpwuid(int(proc['euid'])).pw_name, path, arg, _perm_string(r,w)))
                                
                # If an argument matches a script pattern and location can be found
                for arg in (
                arg for arg in arguments if os.path.isfile(arg) 
                and arg.endswith(script_patterns)):
                    w, r = c.is_writable(arg), c.is_readable(arg)
                    if w or r:
                         script_match_buf.append("PID %s with EUID %s (%s) running '%s' has file argument matching script pattern '%s' is %s." % (
                         proc['pid'], proc['euid'], pwd.getpwuid(int(proc['euid'])).pw_name, path, arg, _perm_string(r,w)))
            

    
    # Add to report
    section.add_issue('Found %s running processeses with writable programs or file'
    ' arguments.' % len(writable_buf))
    section.add_issue(''.join(['=> '+x+'\n' for x in writable_buf]))
    section.add_issue('Found %s running processeses matching interpreters or script file'
    ' arguments with readable or writable files.' % len(script_match_buf))
    section.add_issue(''.join(['=> '+x+'\n' for x in script_match_buf]))
    
    
    return section


    
