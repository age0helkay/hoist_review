import os
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}

description = '''
Searches file system for hidden elf binaries and elf binaries in hidden directories.
"Anything found must be investigated since its highly unusual for executables to be hidden."
Idea taken from: http://people.redhat.com/sgrubb/security/
'''


def _is_elf(file_path):
    ''' Determines if file is elf by matching first four bytes of the file header. '''
    if not c.is_readable(file_path): return False
    if not os.path.isfile(file_path): return False
    with open(file_path, 'r') as f:
        if f.read(4) == '\x7f\x45\x4c\x46': # match elf file header
            return True
        else:
            return False

    
def main(section):
    for path, dirs, files in os.walk('/'):
        
        for f in files:
            abspath = os.path.join(path,f)
           
            if f.startswith('.') and _is_elf(abspath):
                section.add_issue('=> Found hidden elf binary \'%s\'.' % abspath)
            if '/.' in path and _is_elf(abspath):
                section.add_issue('=> Found elf binary \'%s\' in hidden directory.' % abspath)
              
            
    return section
