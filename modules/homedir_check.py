import os, pwd, itertools
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}

description = """ Plaugin searches all users home directories for known
files that could pose a security risk when either readable or writable. 
Ignores files owned by the user running this tool.

Finally finds all writable and readable files in home directories not 
owned by user running this tool.

Related references: 
http://turbochaos.blogspot.se/2013/11/ghetto-privilege-escalation-with-bashrc.html
http://en.wikibooks.org/wiki/UNIX_Computing_Security/Securing_accounts
http://bash.cyberciti.biz/guide/Startup_scripts
http://www.dba-oracle.com/linux/important_files_directories.htm

TODO:
Maybe ignore UID's <100
check for executable
wildcard matching, eg. .*_history
cat ~/.bash_history
cat ~/.nano_history
cat ~/.atftp_history
cat ~/.mysql_history
cat ~/.php_history
"""

 
# Files that pose a risk when either readable or writable 
home_dir_sensitive_rw = ['.netrc','.ssh/id_rsa','.ssh/id_dsa',
                         '.rhosts', '.forward','.shosts',
                         '.my.cnf', '.ssh/authorized_keys','.bash_history',
                         '.sh_history', '.my.cnf']
# Files that mainly pose a risk when writable 
home_dir_sensitive_w = ['.profile', '.cshrc', '.bash_logout',
                        '.bash_profile', '.bashrc', '.login',
                        '.logout', '.xinitrc', '.xsession',
                        '.zshrc']       
                                           
def main(section):
    
    rw_issues = []
    full_list_buf = []
    readable_total = 0
    writable_total = 0
    for i in pwd.getpwall():
        # Skip self
        if i.pw_uid == os.getuid(): continue 
        # Skip users without shell
        if 'nologin' in i.pw_shell: continue 
        if 'false' in i.pw_shell: continue
        # Skip null dirs
        if '/dev/null' in i.pw_dir: continue 
      
        # Check if actual homedir is writable
        if c.is_writable(i.pw_dir): 
            rw_issues.append('Homedir %s owned by %s is writable.' % (i.pw_dir, c.get_owner(i.pw_dir)))
        
        # Search for defined files in homedir
        for f in home_dir_sensitive_rw:
            if c.is_readable(os.path.join(i.pw_dir, f)):
                rw_issues.append('%s owned by %s is readable.' % (
                os.path.join(i.pw_dir, f), c.get_owner(os.path.join(i.pw_dir, f))))
            if c.is_writable(os.path.join(i.pw_dir, f)):
                rw_issues.append('%s owned by %s is writable.' % (
                os.path.join(i.pw_dir, f), c.get_owner(os.path.join(i.pw_dir, f))))
        for f in home_dir_sensitive_w:
            if c.is_writable(os.path.join(i.pw_dir, f)):
                rw_issues.append('%s owned by %s is writable.' % (
                os.path.join(i.pw_dir, f), c.get_owner(os.path.join(i.pw_dir, f))))
        # Search for all writable files in homedirs
        writable = []
        readable = []
        for dirname, dirnames, filenames in os.walk(i.pw_dir):
            for f in itertools.chain(filenames,dirnames):
                if c.is_writable(os.path.join(dirname, f)):
                    writable.append(os.path.join(dirname, f))
                    writable_total+=1
                if c.is_readable(os.path.join(dirname, f)):
                    readable.append(os.path.join(dirname, f))
                    readable_total+=1
        if len(writable)>0:
            full_list_buf.append(('Writable files in home directory of user %s\n' 
            % (i.pw_name)) + ''.join([('%s\n' % x) for x in writable]))
        if len(readable)>0:
            full_list_buf.append(('Readable files in home directory of user %s:\n' 
            % (i.pw_name)) + ''.join([('%s\n' % x) for x in readable]))
           
    # Add to report
    
    section.add_issue('Found %s critial pre-defined files with weak permissions.' 
    % len(rw_issues))
    section.add_issue(''.join([('=> %s \n' % x) for x in rw_issues]))
    section.add_issue('Full list of readable(%s) and writable(%s) non-critical'
    '(but potentially intresting) files in home directories added to appendix.' % (
    readable_total, writable_total))
    
    section.add_appendix(''.join([x+'\n' for x in full_list_buf]))
    
    return section
    
    
    
