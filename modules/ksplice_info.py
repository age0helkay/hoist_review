import urllib2
import json
import os


properties = {
'requires_root': False,
'requires_network': True
}

description = '''
Queries Oracle ksplice inspector api (https://www.ksplice.com/inspector)
with the kernel version to determine if kernel is vulnerable. Requires net
access and python to be compiled with SSL support.

TODO: Handle errors
'''

def main(section):
    
    # Build kernel info string
    host_info = os.uname()
    kernel_information = '%s\n%s\n%s\n%s' % (
    host_info[0],host_info[4], host_info[2], host_info[3])

    # Query ksplice and parse result
    try:
        content = urllib2.urlopen(
        'https://uptrack.api.ksplice.com/api/1/update-list/',
        kernel_information).read()
    except urllib2.URLError, e:
        # shouldnt log errors to report but for now        
        section.add_issue("[!] - There was an error: %r" % e)
        return section

    ksplice_report = json.loads(content)

    # Filter privesc vulns
    filter_1 = 'privilege escalation'
    privesc_vulns = filter (
    lambda x: filter_1 in x.lower(), ksplice_report['updates'])
    

    # Add sections to report
    section.add_issue('Ksplice inspector reports %s issues total '
    'for this kernel version.' % len(ksplice_report['updates']))
    
    section.add_issue('%s issues total matching filter \'%s\'.'
    % (len(privesc_vulns), filter_1))
    
    section.add_issue(''.join([('=> %s \n' % x.encode('ascii','ignore')) for x in privesc_vulns]))
    
    if len(ksplice_report['updates']) > 0:
        section.add_issue('Full list of issues added to appendix.')
        section.add_appendix(''.join(x.encode('ascii','ignore')+'\n' for x in ksplice_report['updates']))

    return section