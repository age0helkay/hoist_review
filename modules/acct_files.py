import pwd
import lib.common as c
from itertools import chain


properties = {
'requires_root': False,
'requires_network': False
}

description = '''
Module checking account related file permissions.
* Checks for readable/writable shadow and gshadow
* Writable passwd and group file
* Checks for any hashes in passwd
* Checks for readable common shadow backup files
* Checks if others users than root has uid 0


'''

# TODO: Possibly add more common backup locations
sensitive_r = [ '/var/backups/shadow.bak','/var/backups/gshadow.bak'
,'/etc/gshadow-','/etc/shadow-' ]

sensitive_rw = [ '/etc/shadow', '/etc/gshadow', '/etc/sudoers' ]

sensitive_w = [ '/etc/passwd', '/etc/group', '/etc/master.passwd',
               '/etc/security/passwd']


def main(section):
    
    issue_buf = []

    # Check sensitive read
    for path in chain(sensitive_r, sensitive_rw):
        if c.is_readable(path):
             issue_buf.append('File \'%s\' is readable.' % path)
        
    # Check sensitive write
    for path in chain(sensitive_w, sensitive_rw):
        if c.is_writable(path):
            issue_buf.append('File \'%s\' is writable.' % path)
        
    # Check for possible hashes
    # TODO: Identify other auth methods? eg. *NP* for NIS etc
    # Check for external authenticatiton
    # More intelligent method of identifying hashes
    for user in pwd.getpwall():
        if len(user.pw_passwd) > 5: #Should surpass common abbrevations
            issue_buf.append(('Unexpected value \'%s\' found in password column of user %s, '
            'hashes might be stored in /etc/passwd.' % (user.pw_passwd,user.pw_name)))
        if user.pw_uid == 0 and user.pw_name != 'root':
            issue_buf.append(('None root user \'%s\' has uid 0.' % (user.pw_name)))


    section.add_issue('Found %s issues related to account files.' % len(issue_buf))
    section.add_issue(''.join(['=> '+x+'\n' for x in issue_buf]))

    return section
    
