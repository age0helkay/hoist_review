import os
from itertools import chain
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}


description = '''
Module checks for writable files in the standard binary paths.
'''
common_bin_locations = ['/usr/sbin', '/sbin', '/usr/local/bin', 
                        '/usr/local/sbin', '/usr/bin', '/bin', '/usr/libexec']

def main(section):
    
    issue_buf = []
    file_collection = []
    
    file_collection.extend(common_bin_locations)
    
    # Collect files and dirs
    for path, dirs, files in chain.from_iterable(
    os.walk(path) for path in common_bin_locations):
        for fullpath in (os.path.join(path,files) for files in chain(files,dirs)):
            if os.path.islink(fullpath): fullpath = os.path.realpath(fullpath)
            file_collection.append(fullpath)

    # sort out dups
    file_collection = sorted(set(file_collection))
    # check permissions
    for path in [x for x in file_collection if c.is_writable(x)]:
        issue_buf.append('%s is writable.' % path)
    
        
    section.add_issue('Found %s writable files in standard binary paths.' % len(issue_buf))
    section.add_issue(''.join(['=> '+x+'\n' for x in issue_buf]))
        

    return section
    
