import os, stat
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}

description = '''
todo:
    check ldd
    
'''

# List of standard suid and sgid binaries taken from nsa rhel hardening guide
# Guide to the Secure Configuration of Red Hat Enterprise Linux 5, rev 4.2, p32

std_sid_bin = [
'/bin/mount', '/bin/ping', '/bin/ping6', 
'/bin/su', '/bin/umount', '/sbin/mount.nfs', 
'/sbin/mount.nfs4', '/sbin/netreport', '/sbin/pam_timestamp_check', 
'/sbin/umount.nfs', '/sbin/umount.nfs4', '/sbin/unix_chkpwd', 
'/usr/bin/at', '/usr/bin/chage', '/usr/bin/chfn', 
'/usr/bin/chsh', '/usr/bin/crontab', '/usr/bin/gpasswd', 
'/usr/bin/locate', '/usr/bin/lockfile', '/usr/bin/newgrp', 
'/usr/bin/passwd', '/usr/bin/rcp', '/usr/bin/rlogin', 
'/usr/bin/rsh', '/usr/bin/ssh-agent', '/usr/bin/sudo', 
'/usr/bin/sudoedit', '/usr/bin/wall', '/usr/bin/write', 
'/usr/bin/Xorg', '/usr/kerberos/bin/ksu', '/usr/libexec/openssh/ssh-keysign', 
'/usr/libexec/utempter/utempter', '/usr/lib/squid/pam_auth', '/usr/lib/squid/ncsa_auth', 
'/usr/lib/vte/gnome-pty-helper', '/usr/sbin/ccreds_validate', '/usr/sbin/lockdev', 
'/usr/sbin/sendmail.sendmail', '/usr/sbin/suexec', '/usr/sbin/userhelper', 
'/usr/sbin/userisdnctl', '/bin/mount', '/bin/ping', 
'/bin/ping6', '/bin/su', '/bin/umount', 
'/sbin/mount.nfs', '/sbin/mount.nfs4', '/sbin/netreport', 
'/sbin/pam_timestamp_check', '/sbin/umount.nfs', '/sbin/umount.nfs4', 
'/sbin/unix_chkpwd', '/usr/bin/at', '/usr/bin/chage', 
'/usr/bin/chfn', '/usr/bin/chsh', '/usr/bin/crontab', 
'/usr/bin/gpasswd', '/usr/bin/locate', '/usr/bin/lockfile', 
'/usr/bin/newgrp', '/usr/bin/passwd', '/usr/bin/rcp', 
'/usr/bin/rlogin', '/usr/bin/rsh', '/usr/bin/ssh-agent', 
'/usr/bin/sudo', '/usr/bin/sudoedit', '/usr/bin/wall', 
'/usr/bin/write', '/usr/bin/Xorg', '/usr/kerberos/bin/ksu', 
'/usr/libexec/openssh/ssh-keysign', '/usr/libexec/utempter/utempter', '/usr/lib/squid/pam_auth', 
'/usr/lib/squid/ncsa_auth', '/usr/lib/vte/gnome-pty-helper', '/usr/sbin/ccreds_validate', 
'/usr/sbin/lockdev', '/usr/sbin/sendmail.sendmail', '/usr/sbin/suexec', 
'/usr/sbin/userhelper', '/usr/sbin/userisdnctl', '/usr/sbin/usernetctl'
]



def _is_sgid(file_path):
    inode_attributes = os.stat(file_path)
    if (inode_attributes.st_mode & + stat.S_IXGRP)>0:
        return bool(inode_attributes.st_mode & stat.S_ISGID)
    else: return False
    
def _is_suid(file_path):
    inode_attributes = os.stat(file_path)
    return bool(inode_attributes.st_mode & stat.S_ISUID)

def main(section):
    sgid_files = []
    suid_files = []
    nonstd_sid_files = []


    # Collect sgid and suid files
    for root, _, files in os.walk('/'):
        for f in files:
            full_path = os.path.join(root, f)
            try: 
                # Ignore symlinks
                if os.path.islink(full_path):
                    continue
                if _is_sgid(full_path):
                    sgid_files.append(full_path)
                if _is_suid(full_path):
                    suid_files.append(full_path)
                
            except:
                pass
       

    # Test collected files
    for f in suid_files:
        if c.is_writable(f):
            section.add_issue('File %s has SUID bit set and is writable, executes as user %s.' % (f, c.get_owner(f)))
        if f not in std_sid_bin:
            nonstd_sid_files.append('SUID file \'%s\' executes as user \'%s\'' % (f, c.get_owner(f)))
            
    for f in sgid_files:
        if c.is_writable(f):
            section.add_issue('File %s has SGID bit set and is writable, executes as group %s.' % (f, c.get_group(f)))
        if f not in std_sid_bin:
            nonstd_sid_files.append('SGID file \'%s\' executes as group \'%s\'' % (f, c.get_group(f)))
            
    if len(nonstd_sid_files)>0:
        section.add_issue('\nFound %s nonstandard sid files:' % len(nonstd_sid_files))
        section.add_issue(''.join(['=> '+x+'\n' for x in nonstd_sid_files]))
        
    
    section.add_issue('Full list of found SGID and SUID files added to appendix.')
    
    section.add_appendix('All files with SUID bit set and their owner.')
    section.add_appendix(''.join(['O: %s - %s \n' % (c.get_owner(x),x) for x in suid_files]))
    section.add_appendix('All files with SGID bit set and their group.')
    section.add_appendix(''.join(['G: %s - %s\n' % (c.get_group(x),x) for x in sgid_files]))
             
             
    
    
    return section
    
    
    
