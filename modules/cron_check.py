import os
import lib.common as c
from itertools import chain


properties = {
'requires_root': False,
'requires_network': False
}


description = """
Module checks for weak file permissions on cron jobs and cron configuration files.
* Checks weak permissions on cron jobs
* Checks weak permissions on cron configuration files
* Checks weak permission och elements in cron path variable
* Checks weak permission on path references in cron jobs and conf files

ref:
http://www.nsa.gov/ia/_files/os/redhat/NSA_RHEL_5_GUIDE_v4.2.pdf, p108
   
TODO:
    * Parse conf files for custom run dirs. Dont know if this is common.
    * Do a better job of parsing cron lines - search for full paths
    * Parse conf commands and check for 
    (sort of does these two allready, just not explicitly)
    * Consider cron.deny/allow
"""

# standard basedirs for cron jobs
cron_basedirs = ['/etc/cron.hourly', '/etc/cron.daily', '/etc/cron.weekly', 
                 '/etc/cron.monthly', '/etc/cron.d', '/var/spool/cron']
                 
# standard conf files (default variant is debian feature)
cron_conf = ['/etc/crontab', '/etc/anacrontab', '/etc/default/anacron', '/etc/default/cron']


def main(section):
    
    issue_buf = []
    info_buf = []
    all_jobs_buf = []
    writable_ref_buf = []
    
    # Check cron confs and basedirs
    for path in chain(cron_conf, cron_basedirs):
        if c.is_writable(path):
            issue_buf.append('%s is writable' % path)
            
    # Chain cron basedirs recursively
    for path, dirs, files in chain.from_iterable(
    os.walk(path) for path in cron_basedirs):
        #join paths for actual cron jobs
        for fullpath in (os.path.join(path,files) for files in files):

            #follow links
            if os.path.islink(fullpath):
                fullpath = os.path.realpath(fullpath)

            # Check writable
            if c.is_writable(fullpath):
                issue_buf.append('Cron job %s is writable. Executes as %s.' % (
                fullpath, c.get_owner(fullpath)))
            
            # Check readable
            if c.is_readable(fullpath):
                all_jobs_buf.append('O: %s - %s' % (
                c.get_owner(fullpath), fullpath))

                #check for writable file refs in cron jobs
                for fileref in (x for x in c.extract_paths(fullpath) if c.is_writable(x)):
                    writable_ref_buf.append('Found ref to writable file %s in file %s owned by %s.' % (
                    fileref, fullpath, c.get_owner(fullpath)))
    
    # examine conf files
    for file_content, path in ((c.read_file(path), path) for path in cron_conf):
       for path_line in (line for line in file_content.split('\n') if 'PATH' in line):
        path_variable = path_line.partition('PATH=')[2]
        
        # get path variable
        info_buf.append('Path defined in %s: %s' % (
        path,path_variable))
        
        # check for writable dirs in path variable
        for defined_path in (x for x in path_variable.split(':') if c.is_writable(x)):
            issue_buf.append('Path \'%s\' defined in \'%s\' PATH variable is writable.' % (defined_path,path))
            
        # check for writable file refs in conf files
        for fileref in (x for x in c.extract_paths(path) if c.is_writable(x)):
            writable_ref_buf.append('Found ref to writable file %s in file %s owned by %s.' % (
            fileref, path, c.get_owner(path)))
        

    # Add findings to report
    
    # Path vars
    section.add_issue(''.join([x+'\n' for x in info_buf]))
    
    # Permission issues with cron files
    section.add_issue('Found %s file permission issues with cron related files.' % len(issue_buf))
    section.add_issue(''.join([('=> %s \n' % x) for x in issue_buf]))
    
    # Permission issues with referenced files
    section.add_issue('Found %s referenced writable paths in cron related files.' % len(writable_ref_buf))
    section.add_issue(''.join([('=> %s \n' % x) for x in writable_ref_buf]))
    
    #All found jobs
    if len(all_jobs_buf):
        section.add_issue(('Full list of readable cron jobs(%s) added to appendix, anything non'
        'standard should be manually audited.' % len(all_jobs_buf)))
        section.add_appendix('Readable cron jobs and their owners:\n'+
        ''.join(x+'\n'for x in all_jobs_buf))
        
    return section


    
