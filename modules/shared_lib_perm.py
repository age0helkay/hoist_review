import os, glob, re
import lib.common as c
from itertools import chain

properties = {
'requires_root': False,
'requires_network': False
}



description = '''
Module checks permissions on all files and directories defined as system wide 
shared libraries in /etc/ld.so.conf.

TODO: check /etc/ld.so.preload
http://minipli.wordpress.com/2009/07/17/ld_preload-vs-etcld-so-preload/
http://blog.tridgell.net/?p=141
http://ellzey.github.io/2014/03/22/preload-reload/

'''

def main(section):
    issue_buf = []
    
    file_collection = []
    # grab all includes
    includes = re.findall(r'include (.*)\b',  c.read_file('/etc/ld.so.conf'))
    
    #change pwd to accomodate relative paths for glob
    prevpwd = os.getcwd()
    os.chdir('/etc')
    
    include_paths = [glob.glob(x) for x in includes]
    
    #flatten list
    include_paths = list(chain.from_iterable(include_paths))
    #Grab all directories included
    ld_directories = list(chain.from_iterable([c.read_comment_free_lines(x)\
    for x in include_paths]))
    
    file_collection.extend([os.path.abspath(x) for x in include_paths])
    file_collection.extend(ld_directories)
    
   
    # create list of all files in included paths
    for path, dirs, files in chain.from_iterable(
    os.walk(path) for path in ld_directories):
        for fullpath in (os.path.join(path,files) for files in chain(files,dirs)):
            if os.path.islink(fullpath): fullpath = os.path.realpath(fullpath)
            file_collection.append(fullpath)
            
     # sort out dups
    file_collection = sorted(set(file_collection))  

    # check permissions
    for path in [x for x in file_collection if c.is_writable(x)]:
        issue_buf.append('%s is writable.' % path)
    
        
    section.add_issue('Found %s permission issues with shared libraries.' % len(issue_buf))
    section.add_issue(''.join(['=> '+x+'\n' for x in issue_buf]))
        
    os.chdir(prevpwd)


    return section
    