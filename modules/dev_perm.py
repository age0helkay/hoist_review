import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}

description = '''
Checks device permissions.
* Checks devices currently mounted
* Checks swaps
* Checks memory
'''

# Filesystems to check
fs = ['ext2', 'ext3', 'ext4', 'reiserfs']

# Helper for info string building
def _perm_string(r,w):
    if r and w:
        return 'readable and writable'
    if r:
        return 'readable'
    if w:
        return 'writable'

def main(section):
    # check mounted devices
    mounts = [x.split(' ') for x in c.read_file('/proc/mounts').split('\n')]
    
    for mount in (x for x in mounts if x[2] in fs):
        dev_path = mount[0]
        mount_point = mount[1]
        r, w = c.is_readable(dev_path), c.is_writable(dev_path)
        if w or r:
            section.add_issue('Device \'%s\' mounted on \'%s\' is %s.' %
            (dev_path, mount_point, _perm_string(r,w)))
            
    # check swaps
    swaps = [x.partition(' ')[0] for x in c.read_file('/proc/swaps').split('\n')[1:]]
    for swap in swaps:
        r, w = c.is_readable(swap), c.is_writable(swap)
        if w or r:
            section.add_issue('Device \'%s\'(swap) is %s.' %
            (swap, _perm_string(r,w)))
    
    # Check mem
    mem_dev = ['/dev/mem', '/dev/kmem']
    for dev in mem_dev:
        r, w = c.is_readable(dev), c.is_writable(dev)
        if w or r:
            section.add_issue('Device \'%s\' is %s.' % 
            (dev, _perm_string(r,w)))
    
    return section
    


