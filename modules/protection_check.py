import os
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}

description = '''
This module checks for protections and various things that affect exploitability.

REF:
    http://7h3ram.blogspot.se/2012/07/exploit-mitigation-techniques-on-linux.html
    http://ubuntuforums.org/showthread.php?t=1398804
    http://www.trapkit.de/tools/checksec.html
    http://hashcrack.org/index.html
    http://www.intel.com.au/content/dam/www/public/us/en/documents/datasheets/3rd-gen-core-desktop-vol-1-datasheet.pdf , p46

    TODO:grlinux?
    execsheld rhel5 p 35, grayhat 241
'''

def main(section):
    

    aslr_status = { 
    '0' : 'ASLR is turned OFF',
    '1' : 'ASLR is turned ON (stack randomization)',
    '2' : 'ASLR is turned ON (stack, heap, and mmap allocation randomization)'
    }
    
    # Check for ALSR    
    # TODO: check Pax?
    aslr_path = '/proc/sys/kernel/randomize_va_space'
    
    if os.path.isfile(aslr_path):
        section.add_issue('ASLR status: ' + aslr_status[c.read_file(aslr_path)])

    
    # Check mmap restriction
    mmap_path = '/proc/sys/vm/mmap_min_addr'
    
    if os.path.isfile(mmap_path):
        mmap_min_addr = c.read_file(mmap_path)
        if mmap_min_addr == '0':
            section.add_issue('mmap allows map to 0')
        else:
            section.add_issue('mmap_min_addr: %s' % mmap_min_addr)
    

    # Check selinux status
    # TODO: fallback on sestatus?      
    selinux_path = '/selinux/enforce'
    if os.path.isfile(selinux_path):
         selinux_enforce = c.read_file(selinux_path)
         if selinux_enforce == '1':
             section.add_issue('SELinux status: SELinux enforces system-wide.')
         elif selinux_enforce == '0':
             section.add_issue('SELinux status: SELinux is permissive.')
    else:
        section.add_issue('SELinux status: not found')
    

    # Check NX/XD support in CPU
    # TODO: PaX?
    
    cpu_path = "/proc/cpuinfo"
    if os.path.isfile(cpu_path):
        # Check nx bit
        if 'nx' in c.read_file(cpu_path):
            section.add_issue('CPU flags: CPU supports NX/XD')
        else:
            section.add_issue('CPU flags: NO NX/XD support in CPU.')
       
        # Check SMEP
        if 'smep' in c.read_file(cpu_path):
            section.add_issue('CPU flags: CPU supports Supervisor Mode Execution Protection (SMEP)')
        else:
            section.add_issue('CPU flags: NO Supervisor Mode Execution Protection (SMEP) support in CPU.')
        

    
    return section

    
    
