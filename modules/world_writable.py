import os
import lib.common as c

properties = {
'requires_root': False,
'requires_network': False
}


description = '''
Module simply finds all world writable directories and files
Checks if directories have sticky bit set.
'''
def _is_sticky(path):
    return os.stat(path).st_mode & 01000 == 01000

def main(section):
    
    islink = os.path.islink
    ignore = ('/run', '/dev', '/proc')
    
    for path in c.recursive_files('/', include_files=True, 
                                  ignored_basedirs=ignore):
        if islink(path): continue # Ignore symlinks
        if c.is_world_writable(path):
            if os.path.isdir(path):
                section.add_issue('=> Directory \'%s\' is world writable. Sticky: %s' % 
                                 (path, _is_sticky(path)))
            else:
                section.add_issue('=> File \'%s\' is world writable.' % path)
            
    return section


    
    
