import os
from datetime import datetime

properties = {
'requires_root': False,
'requires_network': False
}


description = """
Module first tries to determine approximate system install date by checking 
metadata change timestamp(ctime) on /. Then uses this date to compare to mtime
stamps on files in /etc and creates a simple timeline to show configuration
changes since system install.
"""

    
def main(section):
    
    # Try to determine approximate system install date
    for f in os.listdir('/'):
        abs_path = os.path.join('/', f)

        coll = [((abs_path,os.path.getctime(abs_path)))]
        
    approx_system_istall = sorted(coll, key=lambda \
    file_attr: file_attr[1])[:1][0][1]

    section.add_issue(('Guessed approximate system install date: %s\n') %\
    (datetime.fromtimestamp(approx_system_istall).strftime("%d %b %Y %H:%M:%S"))) 
        
   
    # Collect mtime from /etc 
    mod_times = []
    for path, dirs, files in os.walk('/etc'):
        for f in files:
            
            abs_path = os.path.join(path, f)
            if os.path.islink(abs_path): continue # Temp fix. should follow
            mod_times.append((abs_path,os.path.getmtime(abs_path)))
        

    # Filter out files with mtime < sys install, order by date
    mod_times = sorted((x for x in mod_times if x[1]>approx_system_istall), 
                       key=lambda file_attr: file_attr[1], reverse=True)
                       
    # Number of displayed items in report
    n_displayed = 30
    
    
    section.add_issue('Found %s modified files in /etc total since approx system install date.'
    ' %s latest displayed.' % (len(mod_times),n_displayed))
    
    section.add_issue(''.join([datetime.fromtimestamp(x[1])\
    .strftime("%d/%b/%Y %H:%M:%S: ") + x[0] + '\n' for x in mod_times[:n_displayed]]))
        
        
    if n_displayed<len(mod_times):
        section.add_issue('Full list of %s modified files added to appendix' % len(mod_times))
        section.add_appendix(('Full list of modfied files in /etc since %s:' %
        (datetime.fromtimestamp(approx_system_istall).strftime("%d %b %Y %H:%M:%S"))))

        section.add_appendix(''.join([datetime.fromtimestamp(x[1])\
        .strftime("%d/%b/%Y %H:%M:%S: ") + x[0] + '\n' for x in mod_times]))

    return section

