import pkgutil
import lib.reporting
import lib.argparse
import sys

sys.dont_write_bytecode = True
   
def banner():
    banner = r'''                      .--. 
                     |o_o | 
                     |:_/ | 
  _     _  _  ___   //   \ \ 
 | |   | || || _ \ (|     | ) 
 | |__ | __ ||   //'\_   _/`\ 
 |____||_||_||_|_\\___)=(___/ 
 LINUX  ++  HOIST  ++  REVIEW
 '''
    print banner

def arg_custom_actions(additional_arg):
    """
    Custom actions for command line arguments
    """
    class customAction(lib.argparse.Action):
        
        def __call__(self, parser, args, values, option_string=None):
            #print(additional_arg)
            setattr(args, self.dest, values)
            if additional_arg == 'list_modules':
                self.__display_modules__()
               
        def __display_modules__(self):
            """
            Displays availible modules and exits 
            """
            modules = list_modules()
            print 'Available modules(%s):\n' % len(modules)
            print ''.join([' => '+x+'\n' for x in modules])
            exit(0)
    return customAction

def logger(string):
    print string
    
    
def list_modules():
    """ 
    Returns all available modules located in "modules/". 
    """
    return [name for _,name,_ in pkgutil.iter_modules(['modules'])]
    
    
def run_module(module_name):
    """
    Run a module in "modules/" returns module feedback as section object
    """
    module = __import__("modules.%s" % module_name, fromlist=[module_name])  
    module_feedback = lib.reporting.section(module_name)
    # dumb error handling
    try:
        module_feedback = module.main(module_feedback)
    except:
        logger("[!] - Module '%s' failed to run or ran with erors. " % (module_name))
    
    return module_feedback

def parse_arguments():
    """
    Parse arguments
    """
    parser = lib.argparse.ArgumentParser(description='Host review desc?')
   
    parser.add_argument('-l', '--list', help='Lists available modules and exits.', 
                        required=False, type=list_modules, 
                        action=arg_custom_actions('list_modules'),nargs=0)
                       
    parser.add_argument('-o', '--outfile', help='Specifies output file for the '
                        'report. Writes report to stdout by default.', 
                        required=False)
                       
    parser.add_argument('-m', '--modules', help='Specify modules you want to '
                        'run by name. Specify single module or comma separated'
                        ' list', required=False)                    

    args = vars(parser.parse_args())
    return args
    

def main():
    banner()
    args=parse_arguments()

    r = lib.reporting.report()
    
    modules = list_modules()    
    
    # Specific modules chosen
    if args['modules']:
        selected_modules = args['modules'].replace(' ','').split(',')
        modules = filter(lambda x: x in selected_modules, modules)
    
    for idx, module_name in enumerate(modules):
        logger("[*] - Running module (%s/%s) '%s'" % (idx+1,len(modules),module_name))
       
        r.add_section(run_module(module_name))
    
    if args['outfile']:
        out = args['outfile']
    else:
        out = 'stdout'
    r.finalize(out)
    
if __name__ == "__main__":
    try:
        exit(main())
    except KeyboardInterrupt:
        logger.info("Caught keyboard interrupt.")
