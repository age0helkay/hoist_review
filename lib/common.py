import os, re, pwd, grp, stat, itertools


def is_readable(file_path):
    """ 
    Determines if file is readable, mainly wrapper for increased readability 
    """
    return os.access(file_path, os.R_OK)

    
def is_writable(file_path):
    """ 
    Determines if file is writable, mainly wrapper for increased readability 
    """
    return os.access(file_path, os.W_OK)

def is_world_readable(file_path):
    """ 
    Determines if file is world readable, mainly wrapper for increased readability 
    """
    inode_attributes = os.stat(file_path)
    return bool(inode_attributes.st_mode & stat.S_IROTH)

def is_world_writable(file_path):
    """ 
    Determines if file is world writable, mainly wrapper for increased readability 
    """
    inode_attributes = os.stat(file_path)
    return bool(inode_attributes.st_mode & stat.S_IWOTH)

    
def extract_paths(file_path):
    """ 
    Takes a file path and extracts strings that resembles paths from the filecontents 
    """
    # Common false possitives 
    # TODO: Add more fps
    common_fp = ['/dev/null', '/dev/random', '/dev/urandom']
    
    # Read file content
    file_content = read_file(file_path)
    
    # Remove comment lines
    comment_free = ''.join([x+'\n' for x in file_content.split('\n')\
    if not x.startswith('#') ])
    
    # TODO: optimize expression?
    path_matches = re.findall(r'(/[a-zA-Z0-9/\.\-_]*)\b', comment_free)
    # filter out common false possitives
    return [x for x in path_matches if not x in common_fp]


def get_owner(file_path):
    """ Returns the file_paths owners name """
    inode_attributes = os.stat(file_path)
    return pwd.getpwuid(inode_attributes.st_uid).pw_name


def get_group(file_path):
    """ Returns the file_paths groupname """
    inode_attributes = os.stat(file_path)
    return grp.getgrgid(inode_attributes.st_gid).gr_name

      
def read_file(file_path):
    """ 
    Takes file path and returns files contents as string, strips trailing newlines 
    """ 
    if not os.path.exists(file_path): return ''
    if not is_readable(file_path): return ''
    with open(file_path, 'r') as f:
        content = f.read().strip()
    return content


def read_comment_free_lines(file_path):
    """ Reads file path and returns non comment lines as a list """
    content = [line for line in read_file(file_path).split('\n') \
              if not line.startswith('#')] 
    return filter(bool,content)
    
def recursive_files(base_dirs, include_dirs=True, include_files=True, 
                    ignored_basedirs=()):
    """ 
    Takes a base directory or a list of base directories and returns a list of 
    absolute paths all files recursively.
    """ 
    join,chain = os.path.join,itertools.chain.from_iterable
    if isinstance(base_dirs, basestring):
        base_dirs = [base_dirs]
    
    for path, dirs, files in chain(os.walk(path) for path in base_dirs):
        for d in dirs:
            if join(path,d).startswith(ignored_basedirs):
                dirs.remove(d)
        if include_dirs:
            for d in dirs:
                yield join(path,d)
        if include_files:
            for f in files:
                yield join(path,f)

   
                  
    