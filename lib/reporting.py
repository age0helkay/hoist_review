import os
import time

class report(object):
   def __init__(self):
       self.sections = list()
   def add_section(self,section):
       self.sections.append(section)
   def __to_file__(self,report_content, full_path):
       with open(full_path, "w") as report:
           report.write(report_content)
   def finalize(self, out='stdout'):
       # Basic info and timestamp for report header
       host_info = os.uname()
       final_report = 'Report for host "%s"\n' % host_info[1]
       final_report += '%s %s %s %s\n' % (
       host_info[0], host_info[2], host_info[3], host_info[4])
       final_report += 'Completed: %s\n\n' % time.strftime("%c")
       
       #Appendix header
       joint_appendix = '='*80+'\n'
       joint_appendix += 'APPENDIX'.center(80)
       joint_appendix += '\n'+'='*80+'\n\n'
       
       for section in self.sections:
           #handle issues
           final_report += (' Report section for \'%s\' ' % (
           section.module_name.upper())).center(80,'=')
           final_report += '\n'
           if len(section.issues)==0:
               final_report += 'Module reports no issues.'
           else:
               final_report += ''.join((x+'\n') for x in section.issues)

           final_report += '\n\n'
           
           #build appendix
           if len(section.appendix)>0:
               joint_appendix += (' Appendix of module %s ' % (
               section.module_name.upper())).center(80,'=')
               joint_appendix += '\n'
               joint_appendix += ''.join((x+'\n') for x in section.appendix)
           
           
       final_report += joint_appendix

       if out is 'stdout':
           print final_report
       else:
           self.__to_file__(final_report,out)

class section(object):
   def __init__(self, module_name):
       self.module_name = module_name
       self.issues = list()
       self.appendix = list()
   def add_issue(self,issuestring):
       self.issues.append(issuestring)
   def add_appendix(self, appendixstring):
       self.appendix.append(appendixstring)